package com.rapipay.RabbitMq.utilities;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.rapipay.RabbitMq.entities.FunResponseDto;
import com.rapipay.RabbitMq.entities.RequestData;
import com.rapipay.RabbitMq.entities.ResponseData;
import com.rapipay.RabbitMq.respositories.ResponseDataRepository;

public class RestCalls {
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Autowired
	private ResponseDataRepository responseDataRepo;

	
	public void hitFunApi(RequestData requestData) {
		ResponseEntity<FunResponseDto> responseEntity = restTemplate.getForEntity("http://192.168.36.56:8080/fun", FunResponseDto.class);
		System.out.println(responseEntity.getBody());
		
		FunResponseDto funResponseDto = responseEntity.getBody();
		
		ResponseData responseData = new ResponseData();
		Object dataObject = funResponseDto.getData();
		
//		responseData.setRequestDataID();
//		responseData.setStatus();
//		responseData.setResponseData(null);
		
		responseDataRepo.save(responseData);
		
	}
}
