package com.rapipay.RabbitMq.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FunResponseDto {
	private Boolean status;
	private String message;
	private Object data;
}
