package com.rapipay.RabbitMq.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitConfig {
	public final static String QUEUE = "demo.queue";
	public final static String QUEUE_KEY = "queue.key";
	public final static String EXCHANGE = "demo.exchange";
		
	@Bean
	Queue queue() {
		return new Queue(QUEUE, false);
	}

	@Bean
	DirectExchange exchange() {
		return new DirectExchange(EXCHANGE);
	}

	@Bean
	Binding queueBinding() {
		return BindingBuilder.bind(queue()).to(exchange()).with(QUEUE_KEY);

	}

}
