package com.rapipay.RabbitMq.service;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rapipay.RabbitMq.config.RabbitConfig;
import com.rapipay.RabbitMq.entities.RequestData;
import com.rapipay.RabbitMq.respositories.RequestDataRepository;

@Service
public class RequestDataService {

	@Autowired
	private RequestDataRepository requestDataRepository;
	
	private MQProducer producer;
	
	public void requestDataFromApi(RequestData requestDataEntity) {
		requestDataEntity = requestDataRepository.save(requestDataEntity);	
		
		producer.requestDataProducer(requestDataEntity);
	}

	

}
