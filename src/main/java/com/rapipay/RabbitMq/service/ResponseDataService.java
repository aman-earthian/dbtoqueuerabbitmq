package com.rapipay.RabbitMq.service;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.rapipay.RabbitMq.config.RabbitConfig;
import com.rapipay.RabbitMq.entities.RequestData;
import com.rapipay.RabbitMq.respositories.ResponseDataRepository;
import com.rapipay.RabbitMq.utilities.RestCalls;

@Service
public class ResponseDataService {

	@Autowired
	private ResponseDataRepository responseDataRepository;
}
