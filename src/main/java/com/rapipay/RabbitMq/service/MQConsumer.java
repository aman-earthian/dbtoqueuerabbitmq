package com.rapipay.RabbitMq.service;

import org.springframework.amqp.rabbit.annotation.RabbitListener;

import com.rapipay.RabbitMq.config.RabbitConfig;
import com.rapipay.RabbitMq.entities.RequestData;
import com.rapipay.RabbitMq.utilities.RestCalls;

public class MQConsumer {

	private RestCalls restCalls;

	@RabbitListener(queues = RabbitConfig.QUEUE)
	public void dataConsumer(RequestData requestData) throws RuntimeException {
		restCalls.hitFunApi(requestData);
	}
}
