package com.rapipay.RabbitMq.service;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

import com.rapipay.RabbitMq.config.RabbitConfig;
import com.rapipay.RabbitMq.entities.RequestData;

@Service
public class MQProducer {
	
	private RabbitTemplate rabbitTemplate ;
	
	public void requestDataProducer(RequestData requestData) {
		rabbitTemplate.convertAndSend(RabbitConfig.EXCHANGE, RabbitConfig.QUEUE_KEY, requestData);
		System.out.println("Queued : " + requestData);
	}
}
