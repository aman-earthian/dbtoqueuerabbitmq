package com.rapipay.RabbitMq.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.rapipay.RabbitMq.entities.RequestData;
import com.rapipay.RabbitMq.service.RequestDataService;

@RestController
public class RequestDataController {
	@Autowired
	private RequestDataService requestDataService;
		
	@PostMapping("/")
	public String requestDataFromApi(@RequestBody RequestData requestData) {
	
//	      ResponseEntity<FunResponseDto> reqData = restTemplate.getForEntity("http://192.168.36.56:8080/fun", FunResponseDto.class);
	      //	      (192.168.36.56:8080/fun", HttpMethod.GET, entity, String.class).getBody();	     
//		System.out.println(requestData);
	
		requestDataService.requestDataFromApi(requestData);
		return "data inserted";
	}
	
}
