package com.rapipay.RabbitMq.respositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.rapipay.RabbitMq.entities.Dlq;

@Repository
public interface DlqRepository extends JpaRepository<Dlq, Integer>{
	
}
