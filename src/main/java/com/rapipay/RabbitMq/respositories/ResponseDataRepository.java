package com.rapipay.RabbitMq.respositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.rapipay.RabbitMq.entities.ResponseData;

@Repository
public interface ResponseDataRepository extends JpaRepository<ResponseData, Integer>{

}
