package com.rapipay.RabbitMq.respositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.rapipay.RabbitMq.entities.RequestData;

@Repository
public interface RequestDataRepository extends JpaRepository<RequestData, Integer>{

}
