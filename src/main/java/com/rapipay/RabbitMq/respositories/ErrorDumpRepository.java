package com.rapipay.RabbitMq.respositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.rapipay.RabbitMq.entities.ErrorDump;

@Repository
public interface ErrorDumpRepository extends JpaRepository<ErrorDump, Integer>{

}
